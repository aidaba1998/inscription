import React from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'
import Emploi from './component/Emploi'
import Home from './component/Home'
import Sidebar from './component/Sidebar'
import Signin from './component/Signin'
import Signup from './component/Signup'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


export default function App() {
  const User=localStorage.getItem('users')
  return (
    <div style={{overflow:'hidden'}}>

    <ToastContainer />
    
      {User ? (
        <div>
        <Sidebar/>
        <Routes>
     
      <Route  path='/home' element={<Home/>}/>
      <Route  path='/emploi' element={<Emploi/>}/>
      <Route  path='*' element={<Navigate to={"/home"}/>}/>
      
      </Routes>
      </div>
      ):(
        <Routes>
      <Route  path='/' element={<Signin/>}/>
      <Route  path='*' element={<Navigate to={"/"}/>}/>
      <Route  path='/signup' element={<Signup/>}/>
     
      </Routes>
      )}
    </div>
  )
}
