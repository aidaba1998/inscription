

import { collection, deleteDoc, doc, getDocs, onSnapshot, query, QuerySnapshot } from 'firebase/firestore';
import React, { useEffect, useState } from 'react'
import { db } from './authfirebase';
import Formemp from './Formemp';
import {MdDelete} from 'react-icons/md'
import Modifiemploi from './Modifiemploi';

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Viewemp from './Viewemp';
import {BsFillPersonFill,BsFillCalendarDateFill} from 'react-icons/bs'
 import {MdFunctions} from 'react-icons/md'
import {FcDepartment} from 'react-icons/fc'
import {BiCategory} from 'react-icons/bi'



export default function Home() {
  const[emploi,setEmploi]=useState([])
  const [firstname,setFirstname]=useState('')
  const [lastname,setLastname]=useState('')
  const [categori,setCategori]=useState('')
  const[matricul,setMatricul]=useState('')
  const[datenaiss,setDatenaiss]=useState('')
  const[fonction,setFonction]=useState('')
  const[local,setLocal]=useState('')
  const[depart,setDepart]=useState('')
  const[echelon,setEchelon]=useState('')
  const[compte,setCompte]=useState('')
  const[banque,setBanque]=useState('')
  const[user,setUser]=useState("")
  useEffect(()=>{
    const q=query(collection(db,"emploi"))
    onSnapshot(q,(querySnapshot)=>{
      setEmploi(querySnapshot.docs.map(doc=>({id:doc.id,data:doc.data()})))
    })



  },[])

console.log(user)
  
 
  //delete emploi//
  const handledelete = async (id) => {

    const empdelete = doc(db, "emploi", id)
    console.log(id)
    try{
      await deleteDoc(empdelete, {
        prenom:firstname,
        nom:lastname,
        categorie:categori,
        matricul:matricul,
        datenaisse:datenaiss,
        fonction:fonction,
        localisation:local,
        departement:depart,
        echelon:echelon,
        compte:compte,
        banque:banque,
      })
      let option= {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        }
        toast.success("suppresion est réussie",option)
     
    } catch (err) {
      let option= {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        }
        toast.error(" erreur de suppresion",option)
    }    
  }
 
  
  return (
    <div>
    <div className="container mt-5 mb-3">
   <div className='row  justify-content-center '>
   <div className='col-6 '> 
   <h3  className='me-5 mt-3'>Listes des contacts</h3>
   </div>
   <div className='col-6 d-flex justify-content-end pb-3 ' > 
   <Formemp/>

   </div>

   </div>
  
</div>
<div className='container'>
<div className="row justify-content-center  ">
        
           {emploi.map((emp)=>{
            return(
            <div className=' col-md-4'>
            <div className="card p-3 ms-3  mb-2">
                <div className="d-flex justify-content-between ">

                        
                        <div className="ms-2 c-details">
                            <h6 className="mb-0"> <span><BsFillPersonFill/></span> <span className='ms-2' style={{fontWeight:'bolder'}}>{emp.data.prenom} {emp.data.nom}</span></h6> 
                        </div>
                   
                  
                </div>
                <div className="mt-3">
                    <h6 className="heading"><span><BsFillCalendarDateFill/></span><span className='ms-3' >{emp.data.datenaisse}</span></h6>
                    
                </div>
                <div className="mt-3">
                    <h6 className="heading"><span><MdFunctions/></span><span className='ms-3' >{emp.data.fonction}</span></h6>
                   
                    </div>
                    <div className="mt-3">
                    <h6 className="heading"><span><FcDepartment/></span><span className='ms-3' >{emp.data.departement}</span></h6>

                    </div>
                    <div className="mt-3">
                    <h6 className="heading"><span><BiCategory/></span><span className='ms-3' >{emp.data.categorie}</span></h6>

                    </div>
                   
                <div className="mt-3  d-flex justify-content-center">
                <Modifiemploi emploi={emp} />
                <button className='ms-2' onClick={()=>handledelete(emp.id)} style={{fontWeight:'700', border:'1px solid red',background:'transparent',borderRadius:'2px',color:'red'}} >Delete</button>
               <Viewemp emploi={emp} />
                </div>
               
            </div>
            </div>

            )
           })}
           
       
        
    
    </div>

</div>
  

    </div>
  )
}
