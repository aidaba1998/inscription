
import { initializeApp } from "firebase/app";
import { getAuth, GithubAuthProvider, GoogleAuthProvider, signInWithPopup } from  'firebase/auth'

import {getFirestore} from 'firebase/firestore'
import { useNavigate } from "react-router-dom";

const firebaseConfig = {
  apiKey: "AIzaSyDVm3A6VfP7P0LMo1xBg49uHvYxzWm00n8",
  authDomain: "inscription-fffbf.firebaseapp.com",
  projectId: "inscription-fffbf",
  storageBucket: "inscription-fffbf.appspot.com",
  messagingSenderId: "921231649208",
  appId: "1:921231649208:web:2f04ff461267e0c2ba874d",
  measurementId: "G-46G6PWN3DP"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
 export const db = getFirestore(app) 

 export const auth = getAuth(app);
    
 const provider=new GoogleAuthProvider()
  export const  SigninWithGoogle= ()=>{
  
    signInWithPopup(auth,provider)
   
    .then((result)=>{
      
      const {email, uid} = result.user
      console.log(email, uid);
      localStorage.setItem('users',JSON.stringify(email,uid))
        window.location.reload()
    }).catch((error)=>{
      console.log(error)
       
  })

  }
  const providers=new GithubAuthProvider()
  export const  SigninWithGithub=()=>{

   
    signInWithPopup(auth,providers)
  
    .then((result)=>{
      
      const {email, uid} = result.user
     
      console.log(result);
      localStorage.setItem('users',JSON.stringify(email,uid))
        window.location.reload()
       
    }).catch((error)=>{
        console.log(error);
    })

  }