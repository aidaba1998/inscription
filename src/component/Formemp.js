import { addDoc, collection, setDoc } from 'firebase/firestore';
import React, { useState } from 'react';
import Modal from 'react-modal';
import { useNavigate } from 'react-router-dom';
import { db } from './authfirebase';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const customStyles = {
  content: {
    top: '55%',
    left: '54%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-30%',
    background:'#093545',
    transform: 'translate(-50%, -50%)',
    borderRadius:'20px',
 
   
  },
};


export default function Formemp() {
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);
 const [firstname,setFirstname]=useState('')
 const [lastname,setLastname]=useState('')
 const [categori,setCategori]=useState('')
 const[matricul,setMatricul]=useState(Math.floor((Math.random()*999)+100))
 const[datenaiss,setDatenaiss]=useState('')
 const[fonction,setFonction]=useState('')
 const[local,setLocal]=useState('')
 const[depart,setDepart]=useState('')
 const[echelon,setEchelon]=useState('')
 const[compte,setCompte]=useState('')
 const[banque,setBanque]=useState('')

 
   function openModal() {
    setIsOpen(true);
  }


  function closeModal() {
    setIsOpen(false);
  }
  const navigate=useNavigate()
 const handlesubmit= async(e)=>{
  e.preventDefault()
 
  try{
    await addDoc(collection(db,"emploi"),{
      prenom:firstname,
      nom:lastname,
      matricul:matricul,
      categorie:categori,
      fonction:fonction,
      localisation:local,
      departement:depart,
      compte:compte,
      banque:banque,
      echelon:echelon,
      datenaisse:datenaiss
    })
        
    setIsOpen(false)
    let option= {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      }
      toast.success("ajoute est réussie",option)
    
     
  }catch(error){
    console.log(error)
    let option= {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      }
      toast.error("Ajouter invalide",option)

  }
 

 }
  return (
    <div>
      <button    className=' me-5 ' onClick={openModal} style={{background:'#093545',borderRadius:'10px',border:'none',height:'50px',boxSizing:'border-box',color:'#fff',fontWeight:'bolder'}}> Ajouter</button>
      <Modal
        isOpen={modalIsOpen}
        
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
       

      >
        <h3 className='text-center' style={{color:'#fff' ,textTransform:'uppercase'}}>Ajoute un emploi</h3>
        
        <form className="row g-3 needs-validation mx-auto justify-content-center" onSubmit={handlesubmit} >
        <div className="col-md-6">
        <div>
        <label class="form-label" style={{color:'#fff'}}> Nom</label>
    <input type="text" className="form-control"   value={lastname} onChange={(e)=>setLastname(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label " style={{color:'#fff'}}>Date de naissance</label>
        <input type="date" className="form-control "  value={datenaiss} onChange={(e)=>setDatenaiss(e.target.value)}  required   style={{borderRadius:'10px',border:'none'}}/>
   
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Categorie</label>
    <input type="number" className="form-control"   value={categori} onChange={(e)=>setCategori(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Departement</label>
    <input type="text" className="form-control"   value={depart} onChange={(e)=>setDepart(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Echelon</label>
    <input type="text" className="form-control"   value={echelon} onChange={(e)=>setEchelon(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
       
        
     
   
  </div>
  <div className="col-md-6">
  <div >
  <label className='form-label' style={{color:'#fff'}}>Prenom</label>
  <input type="text" className="form-control "    value={firstname} onChange={(e)=>setFirstname(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  <div >
  {/* <label className='form-label ' style={{color:'#fff'}}>Matricul</label> */}
  <input type="hidden" className="form-control "    value={matricul} onChange={(e)=>setMatricul(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
     
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Fonction</label>
  <select id="monselect" className="form-select "    value={fonction} onChange={(e)=>setFonction(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}>
  <option valeur="fr">Comptable</option>
   <option valeur="nl">Developpeur</option>
   <option valeur="en">Gestionnaire</option>
  
  </select>
  
    
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Localisation</label>
  <input type="text" className="form-control "   value={local} onChange={(e)=>setLocal(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Compte</label>
  <input type="text" className="form-control "    value={compte} onChange={(e)=>setCompte(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
   <div>
        <label class="form-label" style={{color:'#fff'}}> Banque</label>
    <input type="text" className="form-control" value={banque} onChange={(e)=>setBanque(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
  
   
    
  </div>
  
  <div className="col-md-5  ">
                  <button   className="btn " type="submit" style={{ width: '100%', height: '37px', borderRadius: '5px', background: 'pink', borderColor: 'pink', color: '#093545' ,fontWeight:'bolder'}}>Enregistre 
                 
                  </button>
                </div>
        </form>
      </Modal>
      
    </div>
  );
}


