
import { addDoc, collection, doc, setDoc, updateDoc } from 'firebase/firestore';
import React, { useEffect, useState } from 'react';
import Modal from 'react-modal';
import { db } from './authfirebase';
import {AiOutlineEdit} from 'react-icons/ai'
import { useNavigate  } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';


const customStyles = {
  content: {
    top: '55%',
    left: '54%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-30%',
    background:'#093545',
    transform: 'translate(-50%, -50%)',
    borderRadius:'20px',
  },
};

Modal.setAppElement('#root')
export default function  Modifiemploi(props) {
    // console.log(props.emploi.data.prenom)
    
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

 const [firstname,setFirstname]=useState(props.emploi?.data?.prenom)
 const [lastname,setLastname]=useState(props.emploi?.data?.nom)
 const [categori,setCategori]=useState(props.emploi?.data?.categorie)
 const[matricul,setMatricul]=useState(props.emploi?.data?.matricul)
 const[datenaiss,setDatenaiss]=useState(props.emploi?.data?.datenaisse)
 const[fonction,setFonction]=useState(props.emploi?.data?.fonction)
 const[local,setLocal]=useState(props.emploi?.data?.localisation)
 const[depart,setDepart]=useState(props.emploi?.data?.departement)
 const[echelon,setEchelon]=useState(props.emploi?.data?.echelon)
 const[compte,setCompte]=useState(props.emploi?.data?.compte)
 const[banque,setBanque]=useState(props.emploi?.data?.banque)
 const [loading, setLoading] = useState(false);
   function openModal() {
    setIsOpen(true);
  }

  

  function closeModal() {
    setIsOpen(false);
  }
const navigate=useNavigate()
  const handleUpdate = async (e,id) => {
    e.preventDefault()
   setLoading(true)
    const empupdate = doc(db, 'emploi', id)
    try{
      
      await updateDoc(empupdate, {
        prenom:firstname,
        nom:lastname,
        categorie:categori,
        matricul:matricul,
        datenaisse:datenaiss,
        fonction:fonction,
        localisation:local,
        departement:depart,
        echelon:echelon,
        compte:compte,
        banque:banque,
        
       
        
      })
      let option= {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        }
        toast.success("mis a jour est validé",option)
        setIsOpen(false)
     
     
    } catch (err) {
      let option= {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        }
        toast.error("mis a jour invalide",option)
    }    
  }
  return (
    <div>
      <button     onClick={openModal} style={{width:'70px',border:'1px solid green',background:'transparent',borderRadius:'2px',color:'green',fontWeight:'700'}} > Edit</button>
      <Modal
        isOpen={modalIsOpen}
        
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
        <h2 className='text-center' style={{color:'#fff' ,textTransform:'uppercase'}}>Modifie un emploi</h2>
        
        <form className="row g-3 needs-validation mx-auto justify-content-center" >
        <div className="col-md-6">
        <div>
        <label class="form-label" style={{color:'#fff'}}> Nom</label>
    <input type="text" className="form-control"   value={lastname} onChange={(e)=>setLastname(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label " style={{color:'#fff'}}>Date de naissance</label>
        <input type="date" className="form-control "  value={datenaiss} onChange={(e)=>setDatenaiss(e.target.value)}  required   style={{borderRadius:'10px',border:'none'}}/>
   
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Categorie</label>
    <input type="number" className="form-control"   value={categori} onChange={(e)=>setCategori(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Departement</label>
    <input type="text" className="form-control"   value={depart} onChange={(e)=>setDepart(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Echelon</label>
    <input type="text" className="form-control"   value={echelon} onChange={(e)=>setEchelon(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        <div>
        <label class="form-label" style={{color:'#fff'}}> Banque</label>
    <input type="text" className="form-control" value={banque} onChange={(e)=>setBanque(e.target.value)} required style={{borderRadius:'10px',border:'none'}}/>
        </div>
        
     
   
  </div>
  <div className="col-md-6">
  <div >
  <label className='form-label' style={{color:'#fff'}}>Prenom</label>
  <input type="text" className="form-control "    value={firstname} onChange={(e)=>setFirstname(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  <div >
  <label className='form-label ' style={{color:'#fff'}}>Matricul</label>
  <input type="text" className="form-control "    value={matricul} onChange={(e)=>setMatricul(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
     
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Fonction</label>
  <input type="text" className="form-control "    value={fonction} onChange={(e)=>setFonction(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Localisation</label>
  <input type="text" className="form-control "   value={local} onChange={(e)=>setLocal(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  <div >
  <label className='form-label' style={{color:'#fff'}}>Compte</label>
  <input type="text" className="form-control "    value={compte} onChange={(e)=>setCompte(e.target.value)} required  style={{borderRadius:'10px',border:'none'}}/>
    
  </div>
  
   
    
  </div>
  
  <div className="col-md-5  ">
                  <button   className="btn " onClick={(e)=>handleUpdate(e,props.emploi?.id)} type="submit" style={{ width: '100%', height: '37px', borderRadius: '5px', background: 'pink', borderColor: 'pink', color: '#093545' ,fontWeight:'bolder'}}>Enregistre 
                 
                  </button>
                </div>
        </form>
      </Modal>
      
    </div>
  );
}


