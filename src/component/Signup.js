import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import {FaGithub} from 'react-icons/fa'
import {AiOutlineGoogle} from 'react-icons/ai'
import { SigninWithGithub, SigninWithGoogle ,auth} from './authfirebase'

import { createUserWithEmailAndPassword } from 'firebase/auth'

export default function Signup() {
  const [email, setEmail]=useState('');
   const [password, setPassword]=useState('');
   const [name, setName]=useState('');
 
const navigate=useNavigate()
const User=localStorage.getItem('users')
 const handleSubmit=async(e)=>{
  e.preventDefault()

  try{
    const user= await createUserWithEmailAndPassword(auth,email,password,name);
    localStorage.setItem('users',JSON.stringify(user))
          console.log(user); 
         
           window.location.reload()
  }catch (error){
    console.log(error.message);
  }

 }
 const Github=()=>{

SigninWithGithub();

}
const Google=()=>{
  SigninWithGoogle();
   
}
  return (
    <div  className='container-fluid ' style={{background:'gray',minHeight:'100vh'}}>
    <div className='row p-0 m-0  justify-content-center' >
    <div className=' col-md-4 mt-2'>
    <div className="card" style={{boxShadow:'0px 0px 0px'}} >
  <div className="card-body">
    <h5 className="card-title" style={{color:'black',fontWeight:'700',textAlign:'center'}}>Sign Up</h5>
    <h6 className=" mb-4 mt-2 " style={{textAlign:'center',}}>Already have an account ?  <Link to='/' style={{textDecoration:'none'}}>Sign in</Link></h6>
    <span className='d-flex justify-content-center '><hr style={{width:'400px'}} /></span>
    <div className='d-flex justify-content-center ' >
        <span style={{position:'absolute',left:'60px'  ,zIndex:'2',color:'#fff',fontSize:'20px'}}> <FaGithub/> <span className='ps-2 pb-1' style={{borderRight:'2px solid #fff'}}></span></span>
        <button className="btn  " type="submit"  onClick={Github} style={{width:'80%' ,height:'35px',borderRadius:'5px',background:'black',borderColor:'black',color:'#fff'}}>Continue with Github</button>
    </div>
    <div className='d-flex justify-content-center mt-4 ' >
        <span style={{position:'absolute',left:'60px'  ,zIndex:'2',color:'#fff',fontSize:'20px'}}> <AiOutlineGoogle/> <span className='ps-2 pb-1' style={{borderRight:'2px solid #fff'}}></span> </span>
        <button className="btn   " type="submit"  onClick={Google} style={{width:'80%' ,height:'35px',borderRadius:'5px',background:'red',borderColor:'red',color:'#fff'}}>Continue with Google</button>
    </div>
    <p className=" d-flex justify-content-center mt-2"><span><hr style={{width:'150px'}}/></span><span className='ms-2'>or</span><span className='ms-2'><hr style={{width:'150px'}}/></span></p>
   <form onSubmit={handleSubmit} className='my-1'>
   <div className="mb-1">
  <label  className="form-label">Full Name: *</label>
  <input type="text" className="form-control" name='name' value={name} onChange={(e)=>setName(e.target.value)} id="formGroupExampleInput" style={{height:'35px',borderRadius:'5px'}} />
</div>
<div className="mb-1">
  <label  className="form-label">Email: *</label>
  <input type="email" className="form-control" name='email' value={email} onChange={(e)=>setEmail(e.target.value)} id="formGroupExampleInput2" style={{height:'35px',borderRadius:'5px'}}/>
</div>
<div className="mb-2">
  <label  className="form-label">Password: *</label>
  <input type="password" className="form-control" name='password' value={password} onChange={(e)=>setPassword(e.target.value)} id="formGroupExampleInput3" style={{height:'35px',borderRadius:'5px'}} />
</div>
 <div className="col-12 ">
    <button className="btn " type="submit"  style={{width:'100%' ,height:'35px',borderRadius:'5px',background:'#ff9933',borderColor:'#ff9933',color:'#fff'}}>SINGN UP</button>
  </div>
   </form>
  

   <p className='text-center' style={{color:'gray'}}>By continuing, you agree to our <Link to='#'> Term of service </Link> and <Link to='#'>  Privacy Policy</Link></p>
  </div>
</div>

    </div>
       
     

    </div>

    </div>
  )
}
