import React, { useState } from 'react';
import Modal from 'react-modal';

import { GrView } from 'react-icons/gr'

import 'react-toastify/dist/ReactToastify.css';


const customStyles = {
    content: {
        top: '55%',
        left: '54%',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-30%',
        background: '#093545',
        transform: 'translate(-50%, -50%)',
        borderRadius: '20px',
        width:'40%'
    },
};

Modal.setAppElement('#root')
export default function Viewemp(props) {
    // console.log(props.emploi.data.prenom)

    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);



    function openModal() {
        setIsOpen(true);
    }



    function closeModal() {
        setIsOpen(false);
    }

    return (
        <div>
            <button className='ms-2' onClick={openModal} style={{width:'70px',border:'1px solid blue',background:'transparent',borderRadius:'2px',color:'blue',fontWeight:'700'}} > View</button>
            <Modal
                isOpen={modalIsOpen}

                onRequestClose={closeModal}
                style={customStyles}
                contentLabel="Example Modal"
            >
                {/* <h2 className='text-center' style={{ color: 'pink', textTransform: 'uppercase' }}>Informations </h2> */}


                <div className="col-md-12 mt-2  ">
                    <div className='d-flex border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}> Prenom:  <p className=' ms-auto'>{props.emploi.data.prenom}</p></div>
                    {/* <hr className=''  style={{color:'#fff'}}/> */}
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Nom: <p className=' ms-auto'>{props.emploi.data.nom}</p> </div>
                    {/* <hr style={{color:'#fff'}}/> */}
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Date de naissance:  <p className=' ms-auto'>{props.emploi.data.datenaisse}</p></div>
                    {/* <hr style={{color:'#fff'}}/> */}
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Matricul: <p className=' ms-auto'> {props.emploi.data.matricul}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Category: <p className=' ms-auto'> {props.emploi.data.categorie}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Fonction: <p className=' ms-auto'> {props.emploi.data.fonction}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Departement: <p className=' ms-auto'> {props.emploi.data.departement}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Localisation: <p className=' ms-auto'> {props.emploi.data.localisation}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Compte: <p className=' ms-auto'> {props.emploi.data.compte}</p></div>
                    <div className='d-flex  border-bottom' style={{ color: '#fff', letterSpacing: '2px' }}>Banque: <p className=' ms-auto'> {props.emploi.data.banque}</p></div>
                    <div className='d-flex' style={{ color: '#fff', letterSpacing: '2px' }}>Echelon: <p className=' ms-auto'> {props.emploi.data.echelon}</p></div>








                </div>



            </Modal>

        </div>
    );
}


