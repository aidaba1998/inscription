import React, { useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import { GrFormNextLink } from 'react-icons/gr'
import { FaGithub } from 'react-icons/fa'
import { AiOutlineGoogle } from 'react-icons/ai'
import { SigninWithGithub, SigninWithGoogle, auth } from './authfirebase'
import { signInWithEmailAndPassword } from 'firebase/auth'
import ReactLoading from 'react-loading';

export default function Signin({ type, color }) {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate()
  const User = localStorage.getItem('users')
  console.log(User)
  const handleLogin = async (e) => {
    setLoading(true)
    e.preventDefault()


    try {
      const user = await signInWithEmailAndPassword(auth, email, password);
      localStorage.setItem('users', JSON.stringify(user))
      console.log(user);

      window.location.reload()
    } catch (error) {
      console.log(error.message);

    }


  };

  const Github = () => {
    SigninWithGithub();
  
   }
  const Google = () => {
    SigninWithGoogle()
    
   }


  return (
    <div className='container-fluid ' style={{ background: 'gray', minHeight: '100vh' }}>
      <div className='row p-0 m-0  justify-content-center' >
        <div className=' col-md-4 mt-5'>
          <div className="card" style={{ boxShadow: '0px 0px 0px' }} >
            <div className="card-body">
              <h5 className="card-title" style={{ color: 'black', fontWeight: '700', textAlign: 'center' }}>Sign In</h5>
              <h6 className=" mb-4 mt-2 " style={{ textAlign: 'center', }}>Not registered yet?  <Link to='/signup' style={{ textDecoration: 'none' }}>Sign up</Link></h6>
              <span className='d-flex justify-content-center'><hr style={{ width: '400px' }} /></span>
              <div className='d-flex justify-content-center ' >
                <span style={{ position: 'absolute', left: '60px', zIndex: '2', color: '#fff', fontSize: '20px' }}> <FaGithub /> <span className='ps-2 pb-1' style={{ borderRight: '2px solid #fff' }}></span></span>
                <button className="btn  " type="submit" onClick={Github} style={{ width: '80%', height: '35px', borderRadius: '5px', background: 'black', borderColor: 'black', color: '#fff' }}>Continue with Github</button>
              </div>
              <div className='d-flex justify-content-center mt-4 ' >
                <span style={{ position: 'absolute', left: '60px', zIndex: '2', color: '#fff', fontSize: '20px' }}> <AiOutlineGoogle /> <span className='ps-2 pb-1' style={{ borderRight: '2px solid #fff' }}></span></span>
                <button className="btn  " type="submit" onClick={Google} style={{ width: '80%', height: '35px', borderRadius: '5px', background: 'red', borderColor: 'red', color: '#fff' }}>Continue with Google</button>
              </div>
              <div className=" d-flex justify-content-center mt-2"><span><hr style={{ width: '150px' }} /></span><span className='ms-2'>or</span><span className='ms-2'><hr style={{ width: '150px' }} /></span></div>
              <form onSubmit={handleLogin}>

                <div className="mb-3">
                  <label className="form-label">Email: </label>
                  <input type="email" className="form-control" name='email' value={email} onChange={(e) => setEmail(e.target.value)} id="formGroupExampleInput" style={{ height: '35px', borderRadius: '5px' }} />

                </div>
                <div className="mb-3">
                  <label className="form-label">Email: </label>
                  <input type="password" className="form-control" name='email' value={password} onChange={(e) => setPassword(e.target.value)} id="formGroupExampleInput2" style={{ height: '35px', borderRadius: '5px' }} />
                </div>


                <div className="col-12  ">
                  <button className="btn " type="submit" style={{ width: '100%', height: '37px', borderRadius: '5px', background: '#ff9933', borderColor: '#ff9933', color: '#fff' }}>Next  <span style={{ color: '#fff', fontWeight: '700' }}><GrFormNextLink /></span>
                    {loading && <div className="spinner-border   text-primary" role="status">
                      <span className="visually-hidden" ></span>
                    </div>}
                  </button>
                </div>
              </form>

            </div>
          </div>

        </div>



      </div>

    </div>
  )
}
