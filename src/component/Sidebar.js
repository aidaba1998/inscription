import * as React from 'react';
import { styled, useTheme, Theme, CSSObject } from '@mui/material/styles';
import Box from '@mui/material/Box';
import MuiDrawer from '@mui/material/Drawer';
import MuiAppBar, { AppBarProps as MuiAppBarProps } from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import CssBaseline from '@mui/material/CssBaseline';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import {FiMenu} from 'react-icons/fi'
import {FaChevronRight,FaChevronLeft} from 'react-icons/fa'
import {BsFillPersonFill} from 'react-icons/bs'
import { Button } from '@mui/material';
import { signOut } from 'firebase/auth';
import {auth} from './authfirebase'
import { useNavigate } from 'react-router-dom';
import {BiSpreadsheet,BiCategory} from 'react-icons/bi'



const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: 'hidden',
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create('width', {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: 'hidden',
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up('sm')]: {
    width: `calc(${theme.spacing(8)} + 1px)`,
  },
});

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));


const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    width: drawerWidth,
    flexShrink: 0,
   
    whiteSpace: 'nowrap',
    boxSizing: 'border-box',
    ...(open && {
      ...openedMixin(theme),
      '& .MuiDrawer-paper': openedMixin(theme),
    }),

    ...(!open && {
      ...closedMixin(theme),
      '& .MuiDrawer-paper': closedMixin(theme),
    }),
  }),
);


export default function Sidebar() {
  const navigate=useNavigate();
  const logout =async()=>{
            try{
                await signOut(auth)
                localStorage.removeItem('users');
    
                window.location.reload()
            }catch (error) {
                console.log(error.message);
              }
           
            };
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}   sx={{ 
         backgroundColor: "#093545",
            color: "#fff",
            fontFamily: 'Montserrat' }} 
        >
      
        <Toolbar>
        
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: 5,
              ...(open && { display: 'none' }),
            }}
          >
            <FiMenu />
            
          </IconButton>
          
          <Typography variant="h6" noWrap component="div">
            Mini  drawer
          </Typography>
          <Typography  sx={{ flexGrow: 1, margin: 0.1, color: 'black' }}>
          </Typography>
          <Button color="inherit" sx={{ justifyContent: 'right' }} onClick={logout} >Deconnecter</Button>

        </Toolbar>
        
      </AppBar>
      <Drawer variant="permanent" open={open}  PaperProps={{
          sx: {
            backgroundColor: "#093545",
            color: "#fff",
            fontFamily: 'Montserrat'
          }
        }}

 >
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose} sx={{ color: '#fff' }} >
            {theme.direction === 'rtl' ? <FaChevronRight  /> : <FaChevronLeft />}
          </IconButton>
         


        </DrawerHeader>
        <Divider />
        <List style={{ marginBottom: 'auto' }}>
        <ListItem  >
              <ListItemButton   onClick={()=>navigate('/emploi')}
                 sx={{ alignItems: 'start', minWidth: 240 }}
              >
                <ListItemIcon sx={{ color: '#fff' }} 
                  
                >
                  <BsFillPersonFill  />
                </ListItemIcon>
               Emploie
                <ListItemText  sx={{ opacity: open ? 1 : 0 }} />
              </ListItemButton>
            </ListItem>
            <Divider />
            <ListItem  >
              <ListItemButton   onClick={()=>navigate('/emploi')}
                 sx={{ alignItems: 'start', minWidth: 240 }}
              >
                <ListItemIcon sx={{ color: '#fff' }} 
                  
                >
                  <BiSpreadsheet  />
                </ListItemIcon>
                Fonction
                <ListItemText  sx={{ opacity: open ? 1 : 0 }} />
              </ListItemButton>
            </ListItem>
            <Divider />
            <ListItem  >
              <ListItemButton   onClick={()=>navigate('/emploi')}
                 sx={{ alignItems: 'start', minWidth: 240 }}
              >
                <ListItemIcon sx={{ color: '#fff' }} 
                  
                >
                  <BiCategory  />
                </ListItemIcon>
                Categorie
                <ListItemText  sx={{ opacity: open ? 1 : 0 }} />
              </ListItemButton>
            </ListItem>
        </List>
       
       
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />
        
        
      </Box>
    </Box>
  );
}
